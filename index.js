//npm i express
// make express application =>
//attached port =>

import express, { json } from "express";
import connectMongodb from "./src/connectdb/connectMongodb.js";
import errorResponse from "./src/helper/errorResponse.js";
import adminRouter from "./src/routes/adminRouter.js";
import productRouter from "./src/routes/productRouter.js";
import reviewRouter from "./src/routes/reviewRouter.js";
import schoolRouter from "./src/routes/schoolRouter.js";
import studentRouter from "./src/routes/studentRouter.js";
import userRouter from "./src/routes/userRouter.js";
import fileRouter from "./src/routes/fileRouter.js";
import { port } from "./src/config/config.js";
import cors from "cors";
import registerRouter from "./src/routes/registerRouter.js";

// localhost:8000/products  method post

let app = express();
app.use(cors());
app.use(json());

app.use("/users", userRouter);
app.use("/schools", schoolRouter);
app.use("/admins", adminRouter);
app.use("/products", productRouter);
app.use("/reviews", reviewRouter);
app.use("/students", studentRouter);
app.use("/file", fileRouter);
app.use("/registers", registerRouter);

app.use(express.static("./public"));
app.use(express.static("./p1"));

app.use(errorResponse);

connectMongodb();

app.listen(port, () => {
  console.log(`express application is listening at port ${port}`);
});

//.env file
//we store confidential information (data)
//port,
//email configuration
//secretKey

//how to set variable in .env file
//how to get variable form .env file

//case
// full name
// camel case =>  fullName
// upper case =>  FULL_NAME
// kaaba case => full-name

//dot env  => to get .env variable
//first install dotenv
//then config dotenv  where .env variable is fetched

//cors
//cross origin resource sharing



//Authenticated

//Authorization
