import { Router } from "express";
import {
  createReview,
  deleteSpecificReview,
  readReview,
  readSpecificReview,
  updateSpecificReview,
} from "../controllers/reviewController.js";

let reviewRouter = Router();

reviewRouter.route("/").post(createReview).get(readReview);

reviewRouter
  .route("/:id")
  .get(readSpecificReview)
  .delete(deleteSpecificReview)
  .patch(updateSpecificReview);

export default reviewRouter;
