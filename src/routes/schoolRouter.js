import { Router } from "express";
import { School } from "../schema/model.js";
import successResponse from "../helper/successResponse.js";
let schoolRouter = Router();

schoolRouter
  .route("/") //localhost:8000/schools
  .post(async (req, res, next) => {
    let data = req.body;

    try {
      let result = await School.create(data);
      successResponse(res, 201, "School created successfully", result);
    } catch (error) {
      console.log(error.message);
    }
  })
  .get(async (req, res, next) => {
    try {
      let result = await School.find({});
      successResponse(res, 200, "read school successfully", result);
    } catch (error) {
      console.log(error.message);
    }
  })
  .patch()
  .delete();

schoolRouter
  .route("/:id")

  .post((req, res, next) => {
    res.json("hello");
  })
  .get(async (req, res, next) => {
    let id = req.params.id;

    try {
      let result = await School.findById(id);
      successResponse(res, 200, "read school detail successfully", result);
    } catch (error) {}
  })

  .delete(async (req, res, next) => {
    let id = req.params.id;

    try {
      let result = await School.findByIdAndDelete(id);
      successResponse(res, 200, "delete school  successfully", result);
    } catch (error) {
      console.log(error.message);
    }
  })
  .patch(async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;

    try {
      let result = await School.findByIdAndUpdate(id, data);
      successResponse(res, 201, "update school detail successfully", result);
    } catch (error) {
      console.log(error.message);
    }
  });

export default schoolRouter;

//dynamic routes

// patch
//id => params
//data => body
