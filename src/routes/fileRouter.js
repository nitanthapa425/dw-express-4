import { Router } from "express";
import upload from "../middleware/uploadFile.js";
import {
  createFile,
  createMultipleFile,
} from "../controllers/fileController.js";
// import {
//   createFile,
//   deleteSpecificFile,
//   readSpecificFile,
//   readFile,
//   updateSpecificFile,
// } from "../controllers/fileController.js";
// import upload from "../middleware/uploadFile.js";

let fileRouter = Router();

fileRouter.route("/single").post(upload.single("file"), createFile);
fileRouter.route("/multiple").post(upload.array("file"), createMultipleFile);

export default fileRouter;
