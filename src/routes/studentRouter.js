import { Router } from "express";
import {
  createStudent,
  deleteSpecificStudent,
  readSpecificStudent,
  readStudent,
  updateSpecificStudent,
} from "../controllers/studentController.js";
import upload from "../middleware/uploadFile.js";

let studentRouter = Router();

studentRouter
  .route("/")
  .post(upload.single("file"), createStudent)
  .get(readStudent);

studentRouter
  .route("/:id")
  .get(readSpecificStudent)
  .delete(deleteSpecificStudent)
  .patch(updateSpecificStudent);

export default studentRouter;
