import { Router } from "express";
import {
  createRegister,
  deleteSpecificRegister,
  getMyProfile,
  loginRegister,
  readRegister,
  readSpecificRegister,
  updateMyProfile,
  updateSpecificRegister,
} from "../controllers/registerControllers.js";
import isAuthenticated from "../middleware/isAuthenticated.js";
import isAuthorization from "../middleware/isAuthorization.js";
let registerRouter = Router();

registerRouter.route("/").post(createRegister).get(readRegister);

registerRouter.route("/login").post(loginRegister);

registerRouter.route("/my-profile").get(isAuthenticated, getMyProfile);
registerRouter
  .route("/my-profile-update")
  .patch(isAuthenticated, updateMyProfile);

registerRouter
  .route("/:id")
  .delete(
    isAuthenticated,
    isAuthorization(["admin", "superAdmin"]),
    deleteSpecificRegister
  );

export default registerRouter;
