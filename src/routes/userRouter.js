import { Router } from "express";
import { User } from "../schema/model.js";
let userRouter = Router();
//localhost:8000/users
userRouter
  .route("/")
  .post(async (req, res, next) => {
    let data = req.body;
    let result = await User.create(data);
    res.status(201).json(result);
  })
  .get(async (req, res, next) => {
    let result = await User.find({});
    res.status(200).json(result);
  })
  .patch()
  .delete();

export default userRouter;

// import { Router } from "express";

//middleware
//req, res, next
//controler => middleare => it doesn not use next()

// //url = route + query
// //route, route parameter
// //route = baseUrl + route parameter
// //query , query parameter

// //   www.facebook.com/user/a? name=nitan&age=29

// export let userRouter = Router();

// userRouter
//   .route("/")
//   .post((req, res) => {
//     console.log(req.query);
//     res.json(req.body);
//   })
//   .get(
//     (req, res, next) => {
//       console.log("i am first");
//       res.json("aaaaaaaaaaaaaaaaaaaa");
//       next();
//     },
//     (req, res, next) => {
//       console.log("i am second");
//       next();
//     },
//     (req, res, next) => {
//       console.log("i am third");
//       next();
//     }
//   )

//   .patch((req, res) => {
//     res.json("i am patch");
//   })
//   .delete((req, res) => {
//     res.json();
//   });

// //statuscode
// //success
// // 2XX
// //post , patch  => 201
// // read, delete => 200

// //error
// // 4XX
// // 404  => not found
