import { Router } from "express";
import {
  createProduct,
  deleteSpecificProduct,
  readProduct,
  readSpecificProduct,
  updateSpecificProduct,
} from "../controllers/productController.js";

let productRouter = Router();

productRouter.route("/").post(createProduct).get(readProduct);

productRouter
  .route("/:id")
  .get(readSpecificProduct)
  .delete(deleteSpecificProduct)
  .patch(updateSpecificProduct);

export default productRouter;
