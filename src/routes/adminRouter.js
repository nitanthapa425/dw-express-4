import { Router } from "express";
import {
  createAdmin,
  deleteSpecificAdmin,
  readAdmin,
  readSpecificAdmin,
  updateSpecificAdmin,
} from "../controllers/adminControllers.js";
let adminRouter = Router();

adminRouter.route("/").post(createAdmin).get(readAdmin);

adminRouter
  .route("/:id")
  .get(readSpecificAdmin)
  .delete(deleteSpecificAdmin)
  .patch(updateSpecificAdmin);

export default adminRouter;

//dynamic routes

// patch
//id => params
//data => body

// User = [
//   { name: "f", age: 9, address: "gagal" },
//   { name: "b", age: 9, address: "sanagau" },
//   { name: "a", age: 2, address: "gagal" },
//   { name: "f", age: 3, address: "sankhu" },
//   { name: "b", age: 5, address: "sanagau" },
//   { name: "f", age: 1, address: "abc" },
// ];

// .find({}).limit("3").skip("2")

// find({}).limit("3");

//find({}).skip("2")

//find({}).skip("2").limit(3)

//find =>object
//sort => ascending, descending , sort
//select => field of object
//skip => skip
//limit("3") => show three document

// expressAsyncHandler;
//it is the try catch wrapper
//if error occure it call next(error)
//the function wrap by  expressAsyncHandler must be async function
