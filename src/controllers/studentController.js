import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Student } from "../schema/model.js";

export let createStudent = expressAsyncHandler(async (req, res, next) => {
  let link = `localhost:8000/${req.file.filename}`;

  let result = await Student.create({
    name: req.body.name,
    age: req.body.age,
    file: link,
  });
  successResponse(res, 201, "Student created successfully", result);
});

export let readStudent = expressAsyncHandler(async (req, res, next) => {
  let result = await Student.find({});
  successResponse(res, 200, "read student successfully", result);
});

export let readSpecificStudent = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Student.findById(id);
  successResponse(res, 200, "read student detail successfully", result);
});

export let updateSpecificStudent = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Student.findByIdAndUpdate(id, data);
    successResponse(res, 201, "update student detail successfully", result);
  }
);

export let deleteSpecificStudent = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Student.findByIdAndDelete(id);
    successResponse(res, 200, "delete student  successfully", result);
  }
);
