//crud

import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Admin } from "../schema/model.js";
import { hashPassword } from "../utils/hashing.js";

export let createAdmin = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;
  let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).{8,15}$/.test(
    data.password
  );
  if (!isValid) {
    let error = new Error(
      "Password must have: at least one lowercase and one uppercase letter, one special character, one digit, and 8-15 characters long and cannot have spaces."
    );
    throw error;
  }

  data.password = await hashPassword(data.password);

  let result = await Admin.create(data);
  successResponse(res, 201, "Admin created successfully", result);
});


export let readAdmin = expressAsyncHandler(async (req, res, next) => {
  let result = await Admin.find({});

  // find({name:"nitan"})
  // select("name age roll")
  // limit("2")
  // skip("3")
  // sort("name")
  // console.log(req.query);
  // {
  //   name:"bindu"
  // }
  // let result = await Admin.find({ name: req.query.name }).select(
  //   req.query.select
  // );
  // let result = await Admin.find({ name: "bindu" });
  // let result = await Admin.find({ name: "bindu", age: 20 });
  // let result = await Admin.find({});
  // let result = await Admin.find({ "location.country": "finland" });

  // let result = await Admin.find({}).select("name email gender -_id");
  // let result = await Admin.find({ gender: "female" }).select(
  //   "name email gender -_id"
  // );

  //selecting field of document
  // let result = await Admin.find({}).select("name location.country -_id");

  //sorting
  // let result = await Admin.find({}).sort("name ");
  // let result = await Admin.find({}).sort("name -age");
  // let result = await Admin.find({}).sort("");

  //limit
  //skip
  // let result = await Admin.find({}).limit("3")//it means only show 3 object
  // let result = await Admin.find({}).skip("3"); //it means skip 3 object
  // let result = await Admin.find({}).skip("2").limit("10"); //it means skip 3 object

  //select
  //select has control over the field of object
  //  -_id means dont show id
  //sort
  //limit
  //skip
  successResponse(res, 200, "read admin successfully", result);
});

export let readSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Admin.findById(id);
  successResponse(res, 200, "read admin detail successfully", result);
});

export let updateSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let data = req.body;
  let result = await Admin.findByIdAndUpdate(id, data);
  successResponse(res, 201, "update admin detail successfully", result);
});

export let deleteSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Admin.findByIdAndDelete(id);
  successResponse(res, 200, "delete admin  successfully", result);
});
