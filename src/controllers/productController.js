import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Product } from "../schema/model.js";
import { sendMail } from "../utils/sendmail.js";

export let createProduct = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;

  await sendMail({
    from: '"Unique" <uniquekc425@gmail.com>',
    to: ["dila.bhusal7@gmail.com", "nitanthapa425@gmail.com"],
    subject: "My first system email",
    html: `<h1>Hello world</h1>`,
  });

  let result = await Product.create(data);
  successResponse(res, 201, "Product created successfully", result);
});

export let readProduct = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.find({});
  successResponse(res, 200, "read product successfully", result);
});

export let readSpecificProduct = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Product.findById(id);
  successResponse(res, 200, "read product detail successfully", result);
});

export let updateSpecificProduct = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Product.findByIdAndUpdate(id, data);
    successResponse(res, 201, "update product detail successfully", result);
  }
);

export let deleteSpecificProduct = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Product.findByIdAndDelete(id);
    successResponse(res, 200, "delete product  successfully", result);
  }
);
