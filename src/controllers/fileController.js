import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";

export let createFile = expressAsyncHandler(async (req, res, next) => {
  let link = `localhost:8000/${req.file.filename}`;

  successResponse(res, 201, "File created successfully", link);
});

export let createMultipleFile = expressAsyncHandler(async (req, res, next) => {
  let links = req.files.map((value, i) => {
    let link = `localhost:8000/${value.filename}`;
    return link;
  });

  successResponse(res, 201, "File created successfully", links);
});
