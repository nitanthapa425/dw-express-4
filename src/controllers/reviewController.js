import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Review } from "../schema/model.js";

export let createReview = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;

  let result = await Review.create(data);
  successResponse(res, 201, "Review created successfully", result);
});

export let readReview = expressAsyncHandler(async (req, res, next) => {
  // let result = await Review.find({});
  // let result = await Review.find({}).populate("productId").populate("adminId");
  let result = await Review.find({}).populate("adminId");
  successResponse(res, 200, "read review successfully", result);
});

export let readSpecificReview = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Review.findById(id);
  successResponse(res, 200, "read review detail successfully", result);
});

export let updateSpecificReview = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Review.findByIdAndUpdate(id, data);
    successResponse(res, 201, "update review detail successfully", result);
  }
);

export let deleteSpecificReview = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Review.findByIdAndDelete(id);
    successResponse(res, 200, "delete review  successfully", result);
  }
);
