//crud

import expressAsyncHandler from "express-async-handler";
import { comparePassword, hashPassword } from "../utils/hashing.js";
import { Register } from "../schema/model.js";
import successResponse from "../helper/successResponse.js";
import { secretKey } from "../config/config.js";
import { generateToken } from "../utils/token.js";

export let createRegister = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;
  data.password = await hashPassword(data.password);
  let result = await Register.create(data);
  successResponse(res, 201, "Register created successfully", result);
});

export let loginRegister = expressAsyncHandler(async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;
  // if password match generate token and send to db

  let user = await Register.findOne({ email: email });
  let dbPassword = user.password;

  let isPasswordMatch = await comparePassword(password, dbPassword);

  if (isPasswordMatch) {
    let info = {
      id: user._id,
      role: user.role,
    };

    let expiryInfo = {
      expiresIn: "365d",
    };

    let token = await generateToken(info, secretKey, expiryInfo);
    successResponse(res, 200, "User login successfully", token);
  } else {
    let error = new Error("Email or Password doesn't match");
    throw error;
  }
});

export let getMyProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let result = await Register.findById(id);
  successResponse(res, 200, "my profile read successfully", result);
});

export let updateMyProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let data = req.body;
  delete data.email;
  delete data.password;
  let result = await Register.findByIdAndUpdate(id, data);
  successResponse(res, 201, "update register detail successfully", result);
});

export let readRegister = expressAsyncHandler(async (req, res, next) => {
  let result = await Register.find({});

  successResponse(res, 200, "read register successfully", result);
});

export let readSpecificRegister = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Register.findById(id);
    successResponse(res, 200, "read register detail successfully", result);
  }
);

export let updateSpecificRegister = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Register.findByIdAndUpdate(id, data);
    successResponse(res, 201, "update register detail successfully", result);
  }
);

export let deleteSpecificRegister = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Register.findByIdAndDelete(id);
    successResponse(res, 200, "delete register  successfully", result);
  }
);
