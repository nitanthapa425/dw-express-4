import mongoose from "mongoose";
import { dbUrl } from "../config/config.js";
let connectMongodb = () => {
  mongoose.connect(dbUrl);
};

export default connectMongodb;
