import { Schema } from "mongoose";

let reviewSchema = Schema({
  productId: {
    type: Schema.ObjectId,
    ref: "Product",
    required: [true, "productId is required"],
  },
  rating: {
    type: Number,
    required: [true, "rating is required"],
  },

  description: {
    type: String,
    required: [true, "description is required"],
  },
  adminId: {
    type: Schema.ObjectId,
    ref: "Admin",
    required: [true, "userID is required"],
  },
});

export default reviewSchema;
