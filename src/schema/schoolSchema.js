import { Schema } from "mongoose";

let schoolSchema = Schema({
  name: {
    type: String,
  },
  location: {
    type: String,
  },
});

export default schoolSchema;
