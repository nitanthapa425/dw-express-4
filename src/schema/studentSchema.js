import { Schema } from "mongoose";

let studentSchema = Schema({
  name: {
    type: String,
  },
  age: {
    type: Number,
  },

  file: {
    type: String,
  },
});

export default studentSchema;

// =>pass Image
// => upload image to server
// => we have link
// => we store that link to the db
