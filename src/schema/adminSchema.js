// {
//   name: "",
//   age: 2,
//   password:"password@123",
//   roll: 2,
//   isMarried: true,
//   spouseName: "lkjl",
//   email: "abc@gmail.com",
//   gender: "male",
//   dob: 2019 - 2 - 2,
//   location: { country: "", exactLocation: "" },
//   favTeacher: ["a", "b", "c"],

//   favSubject: [
//     { bookName: "a", bookAuthor: "a" },
//     { bookName: "b", bookAuthor: "b" },
//   ],
// };

//schema

//manipulation of data

//lowercase:true
//uppercase:true
//trim:true
//unique:true
//default:"male"
//validation

import { Schema } from "mongoose";

//validatin
//required

//fro string
//minLength
//maxLength

// for number
// min
// max

//custom validation
//phone Number

let adminSchema = Schema({
  name: {
    type: String,
    // lowercase: true,
    // uppercase: true,
    // trim: true,
    required: [true, "Name field is required"],
    minLength: [4, "Name field must be at least 4 character"],
    maxLength: [10, "Name field must be at most 10 character"],
    validate: (value) => {
      let isValid = /^[a-zA-Z]+$/.test(value);
      if (isValid === true) {
      } else {
        let error = new Error("Only alphabet is allowed");
        throw error;
      }
    },
  },

  phoneNumber: {
    type: Number,
    required: [true, "Phone Number is required."],
    validate: (value) => {
      let pString = String(value);
      if (pString.length != 10) {
        let error = new Error("Phone Number must be of 10 character");
        throw error;
      }
    },
  },
  password: {
    type: String,
    required: [true, "Password field is required"],
  },
  age: {
    type: Number,
    min: [18, "age must be at least 18"],
    max: [30, "age must be at most 30"],
  },
  roll: {
    type: Number,
  },
  isMarried: {
    type: Boolean,
  },
  spouseName: {
    type: String,

    required: [
      function () {
        if (this.isMarried === true) {
          return true;
        } else {
          this.spouseName = "";
          return false;
        }
      },
      "Spouse name is required.",
    ],

    //isMarried
    //true spouseName:requred true
    //false: spuseName;not requred false
  },
  email: {
    type: String,
    required: [true, "Email field is required"],

    validate: (value) => {
      let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
        value
      );

      if (isValid === true) {
      } else {
        let error = new Error("Email is not valid");
        throw error;
      }
    },
  },
  gender: {
    type: String,
    default: "male",

    enum: {
      values: ["male", "female", "other"],
      message: (notEnum) => {
        return `${notEnum.value} is not valid enum`;
      },
    },
    //if there is default property
    // requited validation does not gives meaning
    // required: [true, "Gender field is required"],

    // validate: (value) => {
    //   // value must be either male, female or other

    //   if (value === "male" || value === "female" || value === "other") {
    //   } else {
    //     let error = new Error("Gender must be either male , female or other");
    //     throw error;
    //   }
    // },
  },
  dob: { type: Date },
  location: { country: { type: String }, exactLocation: { type: String } },
  favTeacher: [
    {
      type: String,
    },
  ],

  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],
});

export default adminSchema;
