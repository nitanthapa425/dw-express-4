import { Schema } from "mongoose";

let registerSchema = Schema({
  fullName: {
    type: String,
    // lowercase: true,
    // uppercase: true,
    // trim: true,
    required: [true, "Name field is required"],
    minLength: [4, "Name field must be at least 4 character"],
    maxLength: [20, "Name field must be at most 10 character"],
  },

  password: {
    type: String,
    required: [true, "Password field is required"],
  },

  email: {
    type: String,
    required: [true, "Email field is required"],

    validate: (value) => {
      let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
        value
      );

      if (isValid === true) {
      } else {
        let error = new Error("Email is not valid");
        throw error;
      }
    },
  },
  role: {
    type: String,
    required: true,
  },
});

export default registerSchema;
